[Home](README.md)

# Base Alpha

[[_TOC_]]

## Overview

This document provides a suggested base setup that can produce the following resources:

* [Rocket ammunition](https://foxhole.fandom.com/wiki/Ammunition_Factory#Rocket_Factory)
* [Large shells](https://foxhole.fandom.com/wiki/Ammunition_Factory#Large_Shell_Factory) (player factory exclusives)
* [Concrete material](https://foxhole.fandom.com/wiki/Concrete_Materials)

![water faclity](images/main.png)

## Resource cost

This section covers the cost of building and maintaining this facility.

### Initial setup cost

We required the following resources from a [Refinery](https://foxhole.fandom.com/wiki/Refinery).

* [Gravel](https://foxhole.fandom.com/wiki/Gravel): 4000
* [Basic Material](https://foxhole.fandom.com/wiki/Basic_Materials): 1000
* [Diesel](https://foxhole.fandom.com/wiki/Diesel): 100

### Player facility resources

We can make the following resources on-site:

* [Construction Materials](https://foxhole.fandom.com/wiki/Construction_Materials): 1600
* [Processed Construction Materials](https://foxhole.fandom.com/wiki/Processed_Construction_Materials): 200

## Planned facilities

This section reviews the planned facilities and their requirements.

### Facility requirements

* 24 [Foundations](https://foxhole.fandom.com/wiki/Foundation)
* 2 [Maintenance tunnel](https://foxhole.fandom.com/wiki/Maintenance_Tunnel)
* 1 [BMS Foreman Stacker](https://foxhole.fandom.com/wiki/Facility_Crane)
* 1 [BMS - Universal Assembly Rig](https://foxhole.fandom.com/wiki/Construction_Vehicle)
* 1 [BMS - Class 2 Mobile Auto-Crane](https://foxhole.fandom.com/wiki/Crane)

### Facilities

* 2 [Power Stations](https://foxhole.fandom.com/wiki/Power_Station) (Coal and Water)
* 1 [Diesel Power Planet](https://foxhole.fandom.com/wiki/Diesel_Power_Plant) (decommission later)
* 25 [Power poles](https://foxhole.fandom.com/wiki/Power_Pole)
* 2 [Coal Refinery](https://foxhole.fandom.com/wiki/Coal_Refinery) (Coal Liquifiers - Concrete production)
* 1 [Oil Refinery](https://foxhole.fandom.com/wiki/Oil_Refinery) (Reformer upgrade - Petrol for coal harvesters)
* 1 [Metal Works Factory](https://foxhole.fandom.com/wiki/Metalworks_Factory) (Recycler)
* 1 [Materials Factory](https://foxhole.fandom.com/wiki/Materials_Factory) (Recycler upgrade)
* 2 [Ammunition Factory](https://foxhole.fandom.com/wiki/Ammunition_Factory) (Rocket and Large Shell)
* 1 [Material Transfer Station](https://foxhole.fandom.com/wiki/Material_Transfer_Station)
* 1 [Resource Transfer Station](https://foxhole.fandom.com/wiki/Resource_Transfer_Station)
* 3 [Liquid Transfer Station](https://foxhole.fandom.com/wiki/Liquid_Transfer_Station) (2 Water) (petrol)

### External water facility

This facility requires water to produce power. We will require a water collection station off-site to provide water. This water station produces 414.720 L every 24 hours. The water collection station will required the following items:

* 2 [Foundations](https://foxhole.fandom.com/wiki/Foundation)
* 4 [Electric Water Pump](https://foxhole.fandom.com/wiki/Water_Pump#Electric_Water_Pump)
* 2 [Liquid Transfer Station](https://foxhole.fandom.com/wiki/Liquid_Transfer_Station) (Water and Diesel)
* 3 [Power poles](https://foxhole.fandom.com/wiki/Power_Pole)
* 1 [BMS Foreman Stacker](https://foxhole.fandom.com/wiki/Facility_Crane)
* 1 [Diesel Power Planet](https://foxhole.fandom.com/wiki/Diesel_Power_Plant)

![water faclity](images/water.png)

## Reoccuring maintenance cost

We will require the following resources every 24 hours:

* [Garrison Supplies](https://foxhole.fandom.com/wiki/Garrison_Supplies) 5000
* [Coal](https://foxhole.fandom.com/wiki/Coal) 115,000
* [Water](https://foxhole.fandom.com/wiki/Water) 48,000 L
* [Salvage](https://foxhole.fandom.com/wiki/Salvage) Based on output requirements.
* [Components](https://foxhole.fandom.com/wiki/Components) Based on output requirements.
