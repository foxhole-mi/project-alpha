[Home](README.md)

# Tech 1 Oil Field Prototype

[[_TOC_]]

## Overview

This blueprint is designed to provide petrol. The blueprint is designed to be [expanded as technology processes](tech2-oil-field.md). 

![oil-field-prototype](/images/teir-1-oil-field.png)

[blueprint json file](/blueprints/tech_1_oil_field_prototype.json)

## Construction Cost

| Amount | Materials|
| ----- | ----- |
| 500 | [Basic Materials](https://foxhole.fandom.com/wiki/Basic_Materials) |
| 1350 | [Gravel](https://foxhole.fandom.com/wiki/Gravel) |
| 575 | [Construction Materials](https://foxhole.fandom.com/wiki/Construction_Materials)|

## Power

* 5 - MW via Diesel power plants.

Note: This power requirement does not include the coal refinery. 

## Garrison Supplies

70 - [Garrison Supplies](https://foxhole.fandom.com/wiki/Garrison_Supplies) per hour required.

## Facilities

| Amount | Facility |
| ----- | -----|
| 6 | [Foundations](https://foxhole.fandom.com/wiki/Foundation) |
| 1 | [Diesel Power Plant](https://foxhole.fandom.com/wiki/Diesel_Power_Plant) |
| 3 | [Oil Refinery](https://foxhole.fandom.com/wiki/Oil_Refinery)  |
| 1 | [Materials Factory](https://foxhole.fandom.com/wiki/Materials_Factory) |
| 1 | [Liquid Transfer Station](https://foxhole.fandom.com/wiki/Liquid_Transfer_Station) |
| 3 | [Oil Well](https://foxhole.fandom.com/wiki/Oil_Well) |
| 1 | [Coal Refinery](https://foxhole.fandom.com/wiki/Coal_Refinery) |
| 1 | [Maintenance Tunnel](https://foxhole.fandom.com/wiki/Maintenance_Tunnel) |
| 5 | [Power Pole](https://foxhole.fandom.com/wiki/Power_Pole) |

