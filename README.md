# Project Alpha

The Project Alpha repository contains Foxhole base blueprints with resource usage overviews.

## Tech 1 Blueprints

* [Tech 1 Oil Field Prototype](tech-1-oil-field.md) - This design is deprecated.

## Tech 2 Blueprints

* [Tech 2 Oil Field Prototype](tech-2-oil-field.md) - This design is deprecated.

## Tech 3 Blueprints

* [Tech 3 Oil Field Prototype](tech-3-oil-field.md)

* [Base Alpha](base-alpha.md)
* [Large Shell Factory](large-shell-factory.md)
* [Oil Field Prototype](oil-field-prototype.md)
