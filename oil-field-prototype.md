[Home](README.md)

# Oil Field Prototype

[[_TOC_]]

## Overview

This blueprint is designed to provide petrol and heavy oil. When fully upgraded, diesel power plants are not required. 

![oil-field-prototype](/images/oil_field_prototype1.png)

[blueprint json file](/blueprints/oil_field_prototype1.json)

## Construction Cost

| Amount | Materials|
| ----- | ----- |
| 625 | [Construction Materials](https://foxhole.fandom.com/wiki/Construction_Materials)|
| 130 | [Processed Construction Materials](https://foxhole.fandom.com/wiki/Processed_Construction_Materials) |
| 600 | [Basic Materials](https://foxhole.fandom.com/wiki/Basic_Materials) |
| 25 | Steel Construction Materials|
| 1500 | [Gravel](https://foxhole.fandom.com/wiki/Gravel) |

## Power

* 10 - MW via Diesel power plants. (Will decommission after the upgrade.)
* 16 - MW via Heavy oil in power plant.

## Garrison Supplies

88 - Garrison Supplies per hour required.

## Facilities

| Amount | Facility |
| ----- | -----|
| 8 | [Foundations](https://foxhole.fandom.com/wiki/Foundation) |
| 2 | [Diesel Power Plant](https://foxhole.fandom.com/wiki/Diesel_Power_Plant) |
| 1 | [Power Station](https://foxhole.fandom.com/wiki/Power_Station) |
| 2 | [Oil Refinery](https://foxhole.fandom.com/wiki/Oil_Refinery)  |
| 1 | [Materials Factory](https://foxhole.fandom.com/wiki/Materials_Factory) |
| 5 | [Liquid Transfer Station](https://foxhole.fandom.com/wiki/Liquid_Transfer_Station) |
| 1 | [Material Transfer Station](https://foxhole.fandom.com/wiki/Material_Transfer_Station) |
| 1 | [BMS Forman Stacker](https://foxhole.fandom.com/wiki/Facility_Crane) |
| 1 | [Maintenance Tunnel](https://foxhole.fandom.com/wiki/Maintenance_Tunnel) |
| 5 | [Power Pole](https://foxhole.fandom.com/wiki/Power_Pole) |
