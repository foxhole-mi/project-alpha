[Home](README.md)

# Large Shell Factory

[[_TOC_]]

## Overview

This document contains the cost and production amount for a Large Shell Factory.

![Large Shell Factory](images/large-shell-factory.png)

## Large Shell Factory vs Mass Production Factory

The [Mass Production Factory](https://foxhole.fandom.com/wiki/Mass_Production_Factory) can only produce `9` [crates](https://foxhole.fandom.com/wiki/Crate) in a single production run. The production run time is variable based on the production queue. If you are at the end of the line in the production queue, it could take the factory over 17 hours to complete the order.

To access the contents of the crates, you must submit the crates to a [Base's](https://foxhole.fandom.com/wiki/Base) stockpile. Then manually pull the ammo out.

The only advantage to using the mass production factory is that you can store the crates in a [Storage Depot's Reserve Stockpile](https://foxhole.fandom.com/wiki/Storage_Depot#Reserve_Stockpiles).

The [Large Shell Factory](https://foxhole.fandom.com/wiki/Ammunition_Factory#Large_Shell_Factory) has no options for crates. Player must transfer the ammo to a [Material Transfer Station](https://foxhole.fandom.com/wiki/Material_Transfer_Station). Then the ammo can be loaded onto crates via a crane.

## Construction cost

| Amount | Material |
| ----- | -----|
| 800 | [Basic Materials](https://foxhole.fandom.com/wiki/Basic_Materials) |
| 400 | [Construction Materials](https://foxhole.fandom.com/wiki/Construction_Materials) |
| 300 | [Processed Construction Materials](https://foxhole.fandom.com/wiki/Processed_Construction_Materials) |
| 1000 | [Gravel](https://foxhole.fandom.com/wiki/Gravel) |

## Facilities

| Amount | Facility |
| ----- | -----|
| 6 | [Foundations](https://foxhole.fandom.com/wiki/Foundation) |
| 2 | [Diesel Power Plant](https://foxhole.fandom.com/wiki/Diesel_Power_Plant) |
| 1 | [Materials Factory](https://foxhole.fandom.com/wiki/Materials_Factory) |
| 1 | [Large Shell Factory](https://foxhole.fandom.com/wiki/Ammunition_Factory#Large_Shell_Factory) |
| 1 | [Liquid Transfer Station](https://foxhole.fandom.com/wiki/Liquid_Transfer_Station) (Diesel) |
| 1 | [Material Transfer Station](https://foxhole.fandom.com/wiki/Material_Transfer_Station) |
| 1 | [Resource Transfer Station](https://foxhole.fandom.com/wiki/Resource_Transfer_Station) |
| 1 | [BMS Forman Stacker](https://foxhole.fandom.com/wiki/Facility_Crane) |
| 1 | [Maintenance Tunnel](https://foxhole.fandom.com/wiki/Maintenance_Tunnel) |
| 5 | [Power Pole](https://foxhole.fandom.com/wiki/Power_Pole) |

## Reoccurring maintenance cost

This base required the following Garrison Supplies every 24 hours:

* 1,400 - [Garrison Supplies](https://foxhole.fandom.com/wiki/Garrison_Supplies)

## 120mm Shell Production

The following resources are needed to produce `240` (`2` [pallets](https://foxhole.fandom.com/wiki/Material_Pallet)) [120mm shells](https://foxhole.fandom.com/wiki/120mm) for a `1` hour period:

| Amount | Material |
| ----- | -----|
| 4,000 L (40 cans)| [Diesel](https://foxhole.fandom.com/wiki/Diesel) |
| 720 | [Explosive Materials](https://foxhole.fandom.com/wiki/Explosive_Materials) |
| 336 | [Construction Materials](https://foxhole.fandom.com/wiki/Construction_Materials) |
