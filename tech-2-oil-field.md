[Home](README.md)

# Tech 2 Oil Field Prototype

[[_TOC_]]

## Overview

This blueprint is designed to expand to a sustainable facility.

![oil-field-prototype](/images/teir-2-oil-field.png)

[blueprint json file](/blueprints/tech_2_oil_field_prototype.json)

## Construction Cost

| Amount | Materials|
| ----- | ----- |
| 550 | [Basic Materials](https://foxhole.fandom.com/wiki/Basic_Materials) |
| 2850 | [Gravel](https://foxhole.fandom.com/wiki/Gravel) |
| 700 | [Construction Materials](https://foxhole.fandom.com/wiki/Construction_Materials) |
| 605 | [Construction Materials](https://foxhole.fandom.com/wiki/Construction_Materials) |

## Power

* 12 - MW via Petrol power plant.
* 10 - MW via Power Station (oil)


Note: This power requirement does not include the coal refinery.

## Garrison Supplies

136 - [Garrison Supplies](https://foxhole.fandom.com/wiki/Garrison_Supplies) per hour required.

## Facilities

| Amount | Facility |
| ----- | -----|
| 19 | [Foundations](https://foxhole.fandom.com/wiki/Foundation) |
| 1 | [Diesel Power Plant](https://foxhole.fandom.com/wiki/Diesel_Power_Plant) Petrol upgrade|
| 3 | [Oil Refinery](https://foxhole.fandom.com/wiki/Oil_Refinery) Cradcking unit upgrade |
| 1 | [Materials Factory](https://foxhole.fandom.com/wiki/Materials_Factory) Smelter upgrade|
| 1 | [Liquid Transfer Station](https://foxhole.fandom.com/wiki/Liquid_Transfer_Station) |
| 3 | [Oil Well](https://foxhole.fandom.com/wiki/Oil_Well) |
| 1 | [Coal Refinery](https://foxhole.fandom.com/wiki/Coal_Refinery) |
| 1 | [Maintenance Tunnel](https://foxhole.fandom.com/wiki/Maintenance_Tunnel) |
| 8 | [Power Pole](https://foxhole.fandom.com/wiki/Power_Pole) |
| 2 | [Metal Works Factory](https://foxhole.fandom.com/wiki/Metalworks_Factory) One blast furnace upgrade |
| 1 | [Ammunition Factory](https://foxhole.fandom.com/wiki/Ammunition_Factory) |
| 1 | [Field Modification Center](https://foxhole.fandom.com/wiki/Field_Modification_Center) |
| 1 | [BMS Foreman Stacker](https://foxhole.fandom.com/wiki/Facility_Crane) |
| 1 | [Material Transfer Station](https://foxhole.fandom.com/wiki/Material_Transfer_Station) |
| 1 | [Light Vehicle Assembly Station](https://foxhole.fandom.com/wiki/Light_Vehicle_Assembly_Station) |
